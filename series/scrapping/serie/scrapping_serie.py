import os
from os import listdir
from os.path import isfile

from bs4 import BeautifulSoup
import requests

import re

import json

regex = re.compile(r'[\n\r\t]')

nb_start = [1,51,101]

with open("serie.json", "w") as fichier_json:

    fichier_json.write("[")

    for nb in nb_start:

        # Address to website imdb.
        URL = 'https://www.imdb.com/search/title/?title_type=tv_series&num_votes=100000,&sort=user_rating,desc&start='+str(nb)+'&ref_=adv_nxt'

        html = requests.get(URL).text

        soup = BeautifulSoup(html, "lxml")

        liste_cadres = soup.find_all("div",{"class":"lister-item"})

        for cadre in liste_cadres:

            # Découpage du cadre de contenu 
            div_content =  cadre.find("div",{"class":"lister-item-content"})
            
            # Récupération du titre
            #######################
            titre = div_content.h3.a.text

            # Récupération de la date de sortie
            ###################################
            date_sortie = div_content.h3.find("span",{"class":"lister-item-year"}).text

            # Récupération du genre
            #######################
            genre_span = div_content.find("span",{"class":"genre"}).text
            genre = regex.sub(" ", genre_span)

            # Récupération du type de public
            ################################
            if (div_content.find("span",{"class":"certificate"})):
                type_public = div_content.find("span",{"class":"certificate"}).text
            else:
                type_public = "Inconnu"

            # Récupération de la note
            #########################
            note = div_content.find("div",{"class":"ratings-imdb-rating"}).strong.text

            # Récupération des acteurs
            ##########################
            liste_element_balise_p = div_content.find_all('p')[2]
            liste_lien_balise_a = liste_element_balise_p.find_all('a')
            acteurs=[]
            for acteur in liste_lien_balise_a:
                acteurs.append(acteur.text)
            acteurs = json.dumps(acteurs)

            # Récupération de la description
            ################################
            description = div_content.find_all('p')[1].text
            description = regex.sub(" ", description)

            # Récupération de la durée moyenne d'un épisode
            ###############################################
            duree_moy = div_content.find("span",{"class":"runtime"}).text

            # Récupération de l'image du cadre 
            ##################################
            div_image = cadre.find("img")
            src_image = div_image["loadlate"]

            fichier_json.write("""
    { 
        "model":"series.serie",
        "fields":
        {
            "title":\""""+titre+ """\",
            "description":\""""+description+"""\",
            "release_date":\""""+date_sortie+"""\",
            "gender":\""""+genre+"""\",
            "duration":\""""+duree_moy+"""\",
            "number_of_episode": "Non scrapper",
            "rating":\""""+note+"""\",
            "public_type":\""""+type_public+"""\",
            "actors":"""+str(acteurs)+""",
            "src_image":\""""+src_image+"""\"
        }
    },""")
        
        fichier_json.write("]")
        
        