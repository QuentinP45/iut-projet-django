from django.db import models


class Director(models.Model):
    lastname_director = models.CharField(max_length = 255)
    firstname_director = models.CharField(max_length = 255)
    #actor = models.BooleanField(Actor, on_delete=models.CASCADE, default=True)

    def __str__(self):
        return self.firstname_director + " " + self.lastname_director

class Actor(models.Model):
    lastname_actor = models.CharField(max_length = 255)
    firstname_actor = models.CharField(max_length = 255)
    #director = models.BooleanField(Director, on_delete=models.CASCADE, default=True)

    def __str__(self):
        return self.firstname_actor + " " + self.lastname_actor

class Gender(models.Model):
    title_gender = models.CharField(max_length = 255)
    description_gender = models.TextField() 

    def __str__(self):
        return self.title_gender



class Serie(models.Model):
    title = models.CharField(max_length = 255)
    description = models.TextField()
    release_date = models.DateField()
    duration = models.IntegerField(default=0) 
    number_of_episode = models.IntegerField(null=True)
    rating = models.FloatField(default=0.0)
    directors = models.ManyToManyField(Director)
    actors = models.ManyToManyField(Actor)
    CHOICES = (
        ('TP',"Tout publics"),
        ('-18',"-18"),
        ('-16',"-16"),
        ('-12',"-12"),
        ('-10',"-10"),
    )
    public_type = models.CharField(max_length=50, choices=CHOICES)

    def __str__(self):
        return self.title


