from django.test import TestCase, Client

# Create your tests here.

from django.urls import resolve
from series.views import *

class app_Test(TestCase):

	def test_fonctionnement_des_test(self):
		self.assertEqual("PL"+"OP","PLOP")

	def test_url_point_entree_appel_home(self):
		found = resolve('/series/serie/home')
		self.assertEqual(found.func, views_serie.home)

	def test_existances_url_listing(self):

		client = Client()
		reponse = self.client.get("/series/series/")
		self.assertEqual(reponse.status_code, 200)
		reponse = self.client.get("/series/actors/")
		self.assertEqual(reponse.status_code, 200)
		reponse = self.client.get("/series/directors/")
		self.assertEqual(reponse.status_code, 200)
		reponse = self.client.get("/series/genders/")
		self.assertEqual(reponse.status_code, 200)


	def test_fonction_url_listing_des_entitees(self):

		found = resolve("/series/series/")
		self.assertEqual(found.func, views_serie.list)
		found = resolve("/series/actors/")
		self.assertEqual(found.func, views_actor.list)
		found = resolve("/series/directors/")
		self.assertEqual(found.func, views_director.list)
		found = resolve("/series/genders/")
		self.assertEqual(found.func, views_gender.list)