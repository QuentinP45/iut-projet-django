from django.urls import path
from .views import *

urlpatterns = [
    #path('serie/home', views_serie.home, name='home'), # test de la route avec son propre views_
    path('', views_serie.list, name="list_series"),
    
    path('actors/', views_actor.list, name="list_actors"),
    path('genders/', views_gender.list, name="list_genders"),
    path('directors/', views_director.list, name="list_directors"),
    # Routes CRUD Serie
    path('serie/create/', views_serie.create, name="create_serie"),
    path('serie/<serie_id>/delete/', views_serie.delete, name="delete_serie"),
    path('serie/<serie_id>/update/', views_serie.update, name="update_serie"),
    path('serie/<serie_id>/details/', views_serie.details, name="details_serie"),
    # Routes CRUD Director
    path('director/create/', views_director.create, name="create_director"),
    path('director/<director_id>/delete/', views_director.delete, name="delete_director"),
    path('director/<director_id>/update/', views_director.update, name="update_director"),
    path('director/<director_id>/details/', views_director.details, name="details_director"),

    # Routes CRUD Actor
    path('actor/create/', views_actor.create, name="create_actor"),
    path('actor/<actor_id>/details/', views_actor.details, name="details_actor"),
    path('actor/<actor_id>/delete/', views_actor.delete, name="delete_actor"),
    path('actor/<actor_id>/update/', views_actor.update, name="update_actor"),
]