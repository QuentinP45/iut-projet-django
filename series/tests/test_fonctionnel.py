from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By

# Fonctionnement de l'accueil de l'application

driver = webdriver.Firefox()

def bouton_retour_accueil(driver):
	"""
	# Retour à http://localhost:8000/
	# Vérifie que le titre de l'onglet est bon
	"""
	driver.find_element(By.LINK_TEXT, "Accueil").click()
	assert driver.title == "Séries"
	return driver

def test_view_serie_from_accueil(driver):
	"""
	Fonction qui vérifie que la page d'accueil permet d'accéder au détail de la
	première série listé, à la modification de la première série, et que la
	suppression fonctionne
	"""

	#L'utilisateur arrive sur le point d'entrée de l'application
	driver.get("http://localhost:8000/")

	# Sur la page d'accueil je peux clicker pour voir le détail de la première série visible :
	driver.find_element(By.XPATH, "/html/body/main/div/div[2]/div[4]/a[1]").click()
	# Je dois me trouver sur une page intitulé Détails Série
	assert driver.title == "Détails Série"
	driver = bouton_retour_accueil(driver)

	# Sur la page d'accueil je peux accéder à la modification d'une série
	driver.find_element(By.XPATH, "/html/body/main/div/div[2]/div[4]/a[2]").click()
	assert driver.title == "Modifier Série"

	# Je reviens à l'accueil
	driver = bouton_retour_accueil(driver)

	# Je garde en mémoire le titre de la première série listée
	first_serie_title = driver.find_element(By.XPATH, "/html/body/main/div/div[2]/div[1]").text
	# Si je clic sur la suppression de la première série :
	driver.find_element(By.XPATH, "/html/body/main/div/div[2]/div[4]/a[3]").click()
	# Elle n'apparait plus dans la liste des série
	assert driver.find_element(By.XPATH, "/html/body/main/div/div[2]/div[1]") != first_serie_title

	return driver

driver = test_view_serie_from_accueil(driver)

print("C'est OK!")

driver.close()
