from django.shortcuts import render, redirect
from django.http import HttpResponse

# modèles importés
from series.models import Director
from series.forms import *

# Create your views here.

def list(request):
    from django.template import Template,Context
    directors = Director.objects.all().order_by('lastname_director')
    
    # TEST de l'affichage liste séries

    # template = Template('{% for director in directors %} {{ director }} <br> {% endfor %}')
    # print(str(template))
    # context=Context({'directors':directors})
    # print(str(template.render(context)))
   
    return render(request , template_name ='director/list_directors.html', context ={'directors':directors})

def create(request):
    from django import forms

    form = Form_director()

    if request.method == "POST":

        form = Form_director(request.POST)

        if form.is_valid():

            new_director = form.save()

        return redirect("/series/directors/")

    title = "Créer Réalisateur"
    action = "Ajouter"
    context = {"form":form, "title":title, "action":action}
    return render(request, "create.html", context)

def delete(request, director_id):
    director = Director.objects.get(pk=director_id)
    director.delete()

    return redirect("/series/directors/")

def update(request, director_id):
    from django import forms

    director = Director.objects.get(pk=director_id)
    form = Form_director(instance=director)

    if request.method == "POST":

        form = Form_director(request.POST, instance=director)

        if form.is_valid():

            new_director = form.save()

        return redirect("/series/directors/")

    title = "Modifier Réalisateur"
    action = "Modifier"
    context = {"form":form, "title":title, "action":action}
    return render(request, "create.html", context)

def details(request, director_id):

    director = Director.objects.get(pk=director_id)
    return render(request, "director/details_director.html", {"director":director})