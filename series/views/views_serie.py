from django.shortcuts import render, redirect
from django.http import HttpResponse

# modèles importés
from series.models import Serie
from series.forms import *


# Create your views here.

# test de la route series/serie/home
def home(request):
    return HttpResponse('Hello projet Django views personnalisée !')

def list(request):
    from django.template import Template,Context
    series = Serie.objects.all().order_by('title')
    
    # TEST de l'affichage liste séries

    # template = Template('{% for serie in series %} {{ serie }} <br> {% endfor %}')
    # print(str(template))
    # context=Context({'series':series})
    # print(str(template.render(context)))
    return render(request , template_name ='serie/list_series.html', context ={'series':series})

def details(request, serie_id):

    serie = Serie.objects.get(pk=serie_id)
    return render(request, "serie/details_serie.html", {"serie":serie})

def create(request):
    from django import forms

    form = Form_serie()

    if request.method == "POST":

        form = Form_serie(request.POST)

        if form.is_valid():

            new_serie = form.save()

        return redirect("/series/")

    title = "Créer Série"
    action = "Ajouter"
    context = {"form":form, "title":title, "action":action}
    return render(request, "create.html", context)

def delete(request, serie_id):

    serie = Serie.objects.get(pk=serie_id)

    serie.delete()

    return redirect("/series/")

def update(request, serie_id):
    from django import forms

    serie = Serie.objects.get(pk=serie_id)
    form = Form_serie(instance=serie)

    if request.method == "POST":

        form = Form_serie(request.POST, instance=serie)

        if form.is_valid():

            new_serie = form.save()

        return redirect("/series/")

    title = "Modifier Série"
    action = "Modifer"
    context = {"form":form, "title":title, "action":action}
    return render(request, "create.html", context)