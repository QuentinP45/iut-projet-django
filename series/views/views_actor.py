from django.shortcuts import render, redirect
from django.http import HttpResponse
from django import forms

# modèles importés
from series.models import Actor
from series.forms import *

# Create your views here.

def list(request):
    from django.template import Template,Context
    actors = Actor.objects.all().order_by('lastname_actor')
    
    # TEST de l'affichage liste acteurs

    # template = Template('{% for actor in actors %} {{ actor }} <br> {% endfor %}')
    # print(str(template))
    # context=Context({'actors':actors})
    # print(str(template.render(context)))
   
    return render(request , template_name ='actor/list_actors.html', context ={'actors':actors})

def create(request):

    form = Form_actor()

    if request.method == "POST":

        form = Form_actor(request.POST)

        if form.is_valid():

            new_actor = form.save()

        return redirect("/series/actors/")

    title = "Créer Acteur"
    action = "Ajouter"
    context = {"form":form, "title":title, "action":action}
    return render(request, "create.html", context)


def details(request, actor_id):

    actor = Actor.objects.get(pk=actor_id)
    return render(request, "actor/details_actor.html", {"actor":actor})


def delete(request, actor_id):

    actor = Actor.objects.get(pk=actor_id)
    actor.delete()
    return redirect("/series/actors/")


def update(request, actor_id):

    actor = Actor.objects.get(pk=actor_id)

    form = Form_actor(instance=actor)

    if request.method == "POST":

        form = Form_actor(request.POST, instance=actor)

        if form.is_valid():

            new_actor = form.save()

        return redirect("/series/actors/")

    title = "Modifier Acteur"
    action = "Modifier"
    context = {"form":form, "title":title, "action":action}
    return render(request, "create.html", context)