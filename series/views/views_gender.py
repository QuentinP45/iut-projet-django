from django.shortcuts import render
from django.http import HttpResponse

# modèles importés
from series.models import Gender

# Create your views here.

def list(request):
    from django.template import Template,Context
    genders = Gender.objects.all().order_by('title_gender')
    
#     # TEST de l'affichage liste séries

#     template = Template('{% for gender in genders %} {{ gender }} <br> {% endfor %}')
#     print(str(template))
#     context=Context({'genders':genders})
#     print(str(template.render(context)))
   
    return render(request , template_name ='gender/list_genders.html', context ={'genders':genders})