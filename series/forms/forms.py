#################################################################
# Fichier qui contiendra les divers formulaires de l'application#
# Si il devient trop gros on le découpera ? #Rémy               #
#################################################################
from series.models import Serie
from series.models import Director
from series.models import Actor
from django.forms import ModelForm
from django import forms

class Form_serie(ModelForm):

	def __init__(self, *args, **kwargs):

		super(Form_serie, self).__init__(*args, **kwargs)
		self.fields['title'].label = "Titre :"
		self.fields['description'].label = "Description :"
		self.fields['release_date'].label = "Date de sortie :"
		self.fields['duration'].label = "Durée épisode :"
		self.fields['rating'].label = "Notes :"
		self.fields['actors'].label = "Têtes d'affiches :"
		self.fields['directors'].label = "Réalisateur :"
		self.fields['public_type'].label = "Type de public :"

	class Meta:
		years = [(year) for year in range(1800,2100)]
		model = Serie
		fields = ('title','description','release_date','duration','rating','actors','directors','public_type')
		widgets = {
			'release_date': forms.SelectDateWidget(years=years)
		}

class Form_director(ModelForm):

	def __init__(self, *args, **kwargs):

		super(Form_director, self).__init__(*args, **kwargs)
		self.fields['lastname_director'].label = "Nom :"
		self.fields['firstname_director'].label = "Prénom :"

	class Meta:
		model = Director
		fields = ('lastname_director','firstname_director')

class Form_actor(ModelForm):

	def __init__(self, *args, **kwargs):

		super(Form_actor, self).__init__(*args, **kwargs)
		self.fields['lastname_actor'].label = "Nom :"
		self.fields['firstname_actor'].label = "Prénom :"

	class Meta:
		model = Actor
		fields = ('lastname_actor','firstname_actor')
