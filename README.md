# Projet Django : iut-projet-django

## Auteurs : ADZUAR Rémy, PICART Quentin, CAMPO Maël

# Versionnage

## Convention de nommage

cf( https://www.grafikart.fr/tutoriels/nommage-commit-1009 )

# Installation

- créer son environnement virtuel : `virtualenv -p python3 venv`
- activer l'environnement virtuel : `source venv/bin/activate`
- migrer la base de données : `./manage.py migrate`
- installer les dependances du projet (bootstrap notamment) :
    - `cd static ` (dans le root)
    - `npm i`

## Lancer le serveur

Commande pour lancer le serveur :
`./manage.py runserver`

## Lancer le scrapping

Une fois dans le dossier ou se situe le fichier de scrapping_serie, utilisez la commande : 

```bash

    python3 scrapping_serie.py

```

Cela va créer/mettre à jour le fichier "serie.json" contenant les informations des séries scrapper.

## Accès à l'application principale
Vous pouvez accéder à l'application en cliquant sur [ce lien](http://127.0.0.1:8000/series/)

## Exécutér les fixtures (jeu de données initiales)
A la racine du projet, ouvrir un terminal et exécuter la commande './manage.py loaddata series/fixtures/\*' pour charger toutes les fixtures crées, ou exécuter la commande './manage.py loaddata series/fixtures/<nom_fichier_fixtures>.json' pour exécuter le jeu de données d'une entité (table de la base de données) spécifique.

## Routes disponibles

```bash

    admin/

    (localhost) /series/series, liste des séries en GET
    (localhost) /series/actors, liste des acteurs en GET
    (localhost) /series/directors, liste des directeur en GET
    (localhost) /series/genders, liste des genres en GET

```

## Installation des dépendances avec npm ou yarn
Se rendre dans le dossier "static" à la racine du projet, ouvrir un terminal et exécuter la commande 'yarn install' (ou 'npm install' en fonction de votre gestionnaire de dépendance installé). Les dépendances listées dans le fichier 
package.json sont installées à la racine du dossier 'node_modules' qui sera crée à l'exécution de la commande.

